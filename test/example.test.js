// example.test.js
const expect = require('chai').expect;
const {assert} = require('chai');
const mylib = require('../src/mylib');

describe("Unit testing mylib.js", () => {

    it("Should return 2 when using sum fucntion with parameters a=1, b=1", ()=> {
        const result = mylib.add(1, 1);
        expect(result).to.equal(2);
    })

    before(() => {
        console.log('Before testing this is printed');
    })

    it('Assert result is positive', () => {
        const result = mylib.substract(10, 15);
        assert(result > 0, 'Result is less than zero, consequently it is negative');
    })

    after(() => {
        console.log('After testing this is printed');
    })

})