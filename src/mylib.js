// src/mylib.js
/**
 * This arrow function returns the sum of two parameters
 * @param {number} param1 this is the first parameter 
 * @param {number} param2 this is the second parameter
 * @returns {number}
 */

const add = (param1, param2) => {
    const result = param1 + param2;
    return result
}

/**
 * This is arrow function too. It is on single line, it has return statement
 * Without curly brackets it doesn't require return statement
 * @param {number} a first param
 * @param {number} b second param
 * @returns 
 */
const substract = (a, b) => a - b

module.exports = {
    add,
    substract,
    random: () => Math.random(),
    arrayGen: () => [1,2,3]
}