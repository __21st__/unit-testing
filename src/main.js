const express = require('express');
const mylib = require('./mylib');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = mylib.add(a, b);
    res.send(sum.toString());
})

app.listen(port, () => {
  console.log(`Server: http://localhost:${port}`)
});